package com.epam;

/**
 * <h1>FibonacciNum</h1>
 * Program build Fibonacci numbers: F1 will be the
 * biggest odd number and F2 – the biggest even
 * number, user can enter the size of set (N). Program
 * prints percentage of odd and even Fibonacci
 * numbers
 */
public class FibonacciNum {

  /**
   * We have lenghtFibonacci private variable.
   */
  private int lenghtFibonacci;
  /**
   * We have massFibonacci private array.
   */
  private int[] massFibonacci;
  /**
   * We have biggestOddNumFibonacci private variable.
   */
  private int biggestOddNumFibonacci;
  /**
   * We have biggestNotOddNumFibonacci private variable.
   */
  private int biggestNotOddNumFibonacci;
  /**
   * We have percent private which equal 100.
   */
  private final int percent = 100;

  /**
   * We have five constructor.
   *
   * @param lenghtFib lenght of Fibonacci number
   */
  public FibonacciNum(final int lenghtFib) {
    this.lenghtFibonacci = lenghtFib;
  }

  /**
   * We have method printFibonacci.
   */
  public final void printFibonacci() {
    massFibonacci = new int[lenghtFibonacci];
    massFibonacci[0] = 0;
    System.out.print(massFibonacci[0] + " ");
    massFibonacci[1] = 1;
    System.out.print(massFibonacci[1] + " ");
    for (int i = 2; i < lenghtFibonacci; ++i) {
      massFibonacci[i] = massFibonacci[i - 1] + massFibonacci[i - 2];
      System.out.print(massFibonacci[i] + " ");
    }
  }

  /**
   * We have method getBiggestOddNumFibonacci.
   *
   * @return biggestOddNumFibonacci return biggest odd number Fibonacci
   */
  public final int getBiggestOddNumFibonacci() {
    if (massFibonacci != null) {
      biggestOddNumFibonacci = massFibonacci[massFibonacci.length - 1];
      return biggestOddNumFibonacci;
    } else {
      System.out.println("Please generate fibonacii array using "
          + "method printFibonacci");
      return 0;
    }

  }

  /**
   * We have method getBiggestNotOddNumFibonacci.
   *
   * @return biggestNotOddNumFibonacci return biggest odd number Fibonacci
   */
  public final int getBiggestNotOddNumFibonacci() {
    if (massFibonacci != null) {
      biggestNotOddNumFibonacci = massFibonacci[massFibonacci.length - 2];
      return biggestNotOddNumFibonacci;
    } else {
      System.out.println("Please generate fibonacci array "
          + "using method printFibonacci");
      return 0;
    }
  }

  /**
   * We have method printPercentOfOddAndNotOddNum.
   */
  public final void printPercentOfOddAndNotOddNum() {
    if (massFibonacci != null) {
      int sumOdd = 0;
      int sumNotOdd = 0;
      for (int i = 0; i < massFibonacci.length; i++) {
        if (massFibonacci[i] == 0) {
          sumNotOdd = sumNotOdd + 1;
          continue;
        }
        if ((massFibonacci[i] % 2) == 0) {
          sumOdd = sumOdd + 1;
        } else if ((massFibonacci[i] % 2) != 0) {
          sumNotOdd = sumNotOdd + 1;
        }
      }
      int percentOdd = (percent * sumOdd) / massFibonacci.length;
      int percentNotOdd = (percent * sumNotOdd) / massFibonacci.length;
      System.out.println("Percent odd numbers: " + percentOdd + "%");
      System.out.println("Percent not odd numbers: " + percentNotOdd + "%");
    } else {
      System.out.println("Please generate fibonacci "
          + "array using method printFibonacci");
    }
  }
}
