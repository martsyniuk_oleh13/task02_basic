package com.epam;
/**
 * <h1>CalculateNum</h1>
 * Program prints odd numbers from start to the end
 * of interval and even from end to start. Program
 * prints the sum of odd and even numbers.
 */
public class CalculateNum {

  /**
   * We have numFrom private field.
   */
  private int numFrom;
  /**
   * We have numTo private field.
   */
  private int numTo;
  /**
   * Constructor.
   */
  public CalculateNum() {
  }
  /** <h1>CalculateNum</h1>.
   * @param numF Number from (start calculating from this number)
   * @param numT Number to (end calculating to this number).
   */
  public CalculateNum(final int numF, final int numT) {
    this.numFrom = numF;
    this.numTo = numT;
  }
  /** <h1>printOddNum</h1>
   * Method which print odd numbers from downstairs to
   * upstairs and from upstairs to downstairs.
   *
   * @param fromTo boolean if true (from downstairs to upstairs)
   *               if false (from upstairs to downstairs)
   */
  public final void printOddNum(final boolean fromTo) {
    if (fromTo) {
      for (int i = numFrom; i <= numTo; i++) {
        if (i % 2 == 0) {
          System.out.print(i + " ");
        }
      }
    } else if (!fromTo) {
      for (int i = numTo; i >= numFrom; i--) {
        if (i % 2 == 0) {
          System.out.print(i + " ");
        }
      }
    }
  }

  /** <h1>printOddNum</h1>
   * Method which print odd numbers from downstairs to,
   *       upstairs and from upstairs to downstairs.
   *
   * @param notOddToo boolean if true (print Sum of Odd numbers),
   *                  if false (print Sum of not Odd numbers)
   */
  public final void printSumOddAndNotOddNumbers(final boolean notOddToo) {
    int sumNotOdd;
    int sumOdd;
    if (notOddToo) {
      sumOdd = 0;
      for (int i = numFrom; i <= numTo; i++) {
        if (i % 2 == 0) {
          sumOdd = sumOdd + i;
        }
      }
      System.out.print("Sum of Odd numbers: " + sumOdd + " ");
    } else if (!notOddToo) {
      sumNotOdd = 0;
      for (int i = numTo; i >= numFrom; i--) {
        if (i % 2 != 0) {
          sumNotOdd = sumNotOdd + i;
        }
      }
      System.out.print("Sum of not Odd numbers: " + sumNotOdd + " ");
    }
  }
}



