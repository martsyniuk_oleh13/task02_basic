/**<h1>Package</h1>
 * @author Martsyniuk Oleh
 * @version 1.0
 */
package com.epam;
/** <h1>My Application</h1>
 * This app calculate odd numbers from start to the
 * end of interval and even from end to start.
 * Also, the sum of odd and even numbers. Program
 * build Fibonacci numbers. After calculation print
 * it in the console. Program prints percentage of
 * odd and even Fibonacci numbers.
 *
 * @author Martsyniuk Oleh
 * @version 1.0
 * @see java.util.Scanner
 * @since 08.11.19
 */
import java.util.Scanner;
/** <h1>Application</h1>
 * class
 * Application.
 */
public class Application {
  /** <h1>Main</h1>.
   * @param args Unused.
   */
  public static void main(final String[] args) {
    /** Method Main it`s Start point of the program.
     * In this method we call two objects , which can
     * calculate numbers.
     * First one is CalculateNum.
     * Second is FibonacciNum.
     *
     * @param args Unused.
     */
    /*In this area we enter 2 numbers and put it in two variables*/
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter first number from: ");
    int from = scanner.nextInt();
    System.out.println("Please enter second number to: ");
    int to = scanner.nextInt();
    /*We create instance of the class CalculateNum and put 2 parameters
    (it is two numbers which we introduced above). And we use all methods
    from this class*/
    CalculateNum calculeteNum = new CalculateNum(from, to);
    calculeteNum.printOddNum(true);
    System.out.print("\n");
    calculeteNum.printOddNum(false);
    System.out.print("\n");
    calculeteNum.printSumOddAndNotOddNumbers(true);
    System.out.print("\n");
    calculeteNum.printSumOddAndNotOddNumbers(false);
    System.out.print("\n");
    /*In this area we enter 1 number (lenght of Fibonacci number) and put it in
    a variable*/
    System.out.println("Please enter fibonacci number : ");
    int fibonacciLenght = scanner.nextInt();
    /*We create instance of the class FibonacciNum and put 1 parameter
    (it is number which we introduced above). And we use all methods
    from this class*/
    FibonacciNum fibonacciNum = new FibonacciNum(fibonacciLenght);
    fibonacciNum.printFibonacci();
    System.out.print("\n");
    System.out.print("Biggest odd number Fibonacci: "
        + fibonacciNum.getBiggestOddNumFibonacci());
    System.out.print("\n");
    System.out.print("Biggest not odd number Fibonacci: "
        + fibonacciNum.getBiggestNotOddNumFibonacci());
    System.out.print("\n");
    fibonacciNum.printPercentOfOddAndNotOddNum();
  }
}
